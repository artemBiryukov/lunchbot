package app;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

public class LunchBot extends TelegramLongPollingBot {
    private static final Map<String, String> DAY_MAP = new HashMap<>();
    private static final HashSet<String> userList = new HashSet<>();

    static {
        DAY_MAP.put("Mon", "c1");
        DAY_MAP.put("Tue", "c2");
        DAY_MAP.put("Wed", "c3");
        DAY_MAP.put("Thu", "c4");
        DAY_MAP.put("Fri", "c5");
        DAY_MAP.put("Sat", "c6");
        DAY_MAP.put("Sun", "c7");
    }

    @Override
    public synchronized void onUpdateReceived(Update update) {
        String chatId = update.getMessage().getChatId().toString();//чат
        String message = update.getMessage().getText();//сообщение

        if (message.equals("/start")) {
            sendMsg(chatId, "Наберите команду /lunch чтобы получить список блюд.");
        }
        if (message.equals("/lunch")) {
            userList.add(chatId);
            sendMsg(chatId, "Подождите, спрашиваем повара...");
            sendMsg(chatId, getLunch());
        }
        if (message.equals("/usersizelist")) {
            sendMsg(chatId, String.valueOf(userList.size()));
        }
    }

    @Override
    public String getBotUsername() {
        return "OlivkaLunchBot";
    }

    @Override
    public String getBotToken() {
        return "986519863:AAEakQPjj-RLS0R2BQsa9HnhZ4kVb6r1Z0Y";
    }

    private synchronized void sendMsg(String chatId, String s) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        sendMessage.setText(s);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private synchronized String getLunch() {
        Date currentDate = new Date();
        SimpleDateFormat df = new SimpleDateFormat("E", Locale.ENGLISH);
        String currentDay = df.format(currentDate);
        String filter = DAY_MAP.get(currentDay);
        StringBuilder sb = new StringBuilder();
        try (WebClient CLIENT = new WebClient(BrowserVersion.CHROME)) {
            CLIENT.getOptions().setDownloadImages(false);
            CLIENT.getOptions().setJavaScriptEnabled(true);
            CLIENT.getOptions().setCssEnabled(false);
            CLIENT.getOptions().setThrowExceptionOnScriptError(false);
            CLIENT.getOptions().setThrowExceptionOnFailingStatusCode(false);
            CLIENT.getCookieManager().setCookiesEnabled(false);
            CLIENT.setJavaScriptErrorListener(new CustomErrorListener());
            URL url = new URL("https://olivkafood.ru/o-kafe/#complex");
            HtmlPage page = CLIENT.getPage(url);
            CLIENT.waitForBackgroundJavaScript(200);
            HtmlPage page1 = page.getAnchorByHref("#complex").click();
            CLIENT.waitForBackgroundJavaScript(200);
            String xpathForName = String.format("//div[@class='menu-item mix menu-category-filter %s']" +
                    "//div[@class='extended-item complex-item']" +
                    "//div[@class='extended-item']" +
                    "//div[@class='item-name']" +
                    "//b", filter);
            List<DomElement> items = page1.getByXPath(xpathForName);
            if (items.isEmpty()) {
                sb.append("Сегодня выходной");
            } else {
                for (DomElement de : items) {
                    sb.append(de.getTextContent()).append('\n');
                }
                sb.setLength(sb.length() - 1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
